#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n = 3;
    vector<vector<int>> initialSequence {{1, 10}, {2, 11}, {3, 11}, {4, 11}, {5, 11}, {6, 10}, {7, 11}, {8, 11},
                                           {9, 11}, {10, 11}, {11, 11}, {12, 11}, {13, 11}, {14, 10}};
    vector<vector<int>> finalSequence;
    int subsequenceCounter = 0;

    for (int i = 0; i < initialSequence.size() - 1; i++) {
        int j = i + 1;
        int nextItem = initialSequence[j][1];

        if(initialSequence[i][1] != nextItem) {
            finalSequence.push_back(initialSequence[i]);
            subsequenceCounter = 0;
        }
        else {
            subsequenceCounter++;
            if(subsequenceCounter % n == 0 || subsequenceCounter == 1) {
                finalSequence.push_back(initialSequence[i]);
            }
        }
    }
    finalSequence.push_back(initialSequence[initialSequence.size()-1]);

    cout << "Исходная: ";
    for(auto &it:initialSequence) {
        cout << "(" << it[0] << ", " << it[1] << ") ";

    }
    cout << endl;

    cout << "Результат при n = " << n << ": ";
    for(auto &it:finalSequence) {
        cout << "(" << it[0] << ", " << it[1] << ") ";

    }
    cout << endl;
    return 0;
}